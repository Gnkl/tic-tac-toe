import React from 'react';
import { Button, ListGroup, ListGroupItem } from 'react-bootstrap';

import Board from './Board';

function calculateWinner(squares) {
  const lines = [
    [0, 1, 2],
    [3, 4, 5],
    [6, 7, 8],
    [0, 3, 6],
    [1, 4, 7],
    [2, 5, 8],
    [0, 4, 8],
    [2, 4, 6],
  ];
  for (let i = 0; i < lines.length; i += 1) {
    const [a, b, c] = lines[i];
    if (squares[a] && squares[a] === squares[b] && squares[a] === squares[c]) {
      return squares[a];
    }
  }
  if (squares.includes(null)) return null;
  return '-';
}


class Game extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      xIsNext: true,
      history: [Array(9).fill(null)],
      winner: null,
      stepNumber: 0,
    };
  }

  handleClick(i) {
    const {
      xIsNext,
      history,
      winner,
      stepNumber,
    } = this.state;
    const newHistory = history.slice(0, stepNumber + 1); // history until stepNumber
    const squares = newHistory.slice(-1).pop().slice(); // copy the last element of the history
    if (squares[i] != null) return;
    if (winner != null) return;
    squares[i] = (xIsNext) ? 'X' : 'O';
    const newWinner = calculateWinner(squares);
    this.setState({
      xIsNext: !xIsNext,
      history: newHistory.concat([squares]),
      winner: newWinner,
      stepNumber: stepNumber + 1,
    });
  }

  handleListClick(stepNumber) {
    this.setState(prevState => ({
      xIsNext: (stepNumber % 2) === 0,
      stepNumber,
      winner: calculateWinner(prevState.history.slice()[stepNumber]),
    }));
  }

  render() {
    const {
      xIsNext,
      history,
      winner,
      stepNumber,
    } = this.state;
    const status = (() => {
      switch (winner) {
        case '-': return 'It is a Draw!';
        case 'X': return 'Winner : X';
        case 'O': return 'Winner : O';
        default: return `Next player: ${xIsNext ? 'X' : 'O'}`;
      }
    })();
    const squares = history[stepNumber];
    const moves = history.map((a, move) => (
      <ListGroupItem bsStyle="warning" key={move}>
        <Button bsStyle="danger" bsSize="large" type="button" className="past-move" onClick={() => this.handleListClick(move)}>
          {`Move ${move}`}
        </Button>
      </ListGroupItem>
    ));

    return (
      <div className="game">
        <div className="game-board">
          <Board squares={squares} onClick={i => this.handleClick(i)} />
        </div>
        <div className="game-info">
          <div className="status">{status}</div>
          <ListGroup>
            {moves}
          </ListGroup>
        </div>
      </div>
    );
  }
}

export default Game;

// ========================================

// function calculateWinner(squares) {
//   const lines = [
//     [0, 1, 2],
//     [3, 4, 5],
//     [6, 7, 8],
//     [0, 3, 6],
//     [1, 4, 7],
//     [2, 5, 8],
//     [0, 4, 8],
//     [2, 4, 6],
//   ];
//   const result = lines.map( array => {
//     if ( squares[array[0]] && squares[array[0]]==squares[array[1]]
//          && squares[array[0]]==squares[array[2]] )
//       return squares[array[0]];
//     return null;
//   }).filter( obj => {
//     if (obj!=null) return obj;
//   });
//   if (result.lenght) return result[0];
//   if ( squares.includes(null) ) return null;
//   return '-';
// }
