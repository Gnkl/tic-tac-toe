import React from 'react';
import { Button } from 'react-bootstrap';

const Square = ({ value, onClick }) => (

  <Button bsStyle="primary" bsSize="small" type="button" className="square" onClick={onClick}>
    {value}
  </Button>
);

export default Square;
